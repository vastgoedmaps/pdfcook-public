# PDFCook

Create advanced PDF document with Chrome Headless


## Installation

```
yarn add 
```

## Usage

```typescript
import * as PDFCook from 'pdfcook';

const exampleRecipe = {
  format: 'A4',
  content: [
    {
      type: 'header',
      variants: ['h1'],
      value: 'My Awesome Title',
    },
    {
      type: 'header',
      variants: ['h2'],
      value: 'An even more awesome Sub-Title',
    },
    {
      type: 'paragraph',
      value: 'Once upon a time ...',
    },
    {
      type: 'html',
       value: `
        <table>
          ...
        </table
       `,
    }
  ],
  layoutElements: [
    {
      type: 'text',
      position: 'bottom-left',
      values: [
        'Nieuwbouw verpleeghuis groene loper Zoetermeer',
        '07-11-2018',
      ],
    },
    {
      type: 'pageNumbers',
      position: 'bottom-right',
      template: '`pagina ${currentPage} van ${totalPages}`',
    },
    {
      type: 'image',
      path: '{imagePath}',
      width: 50,
      height: 50,
      position: 'top-right',
    },
  ],
};

PDFCook.generatePDF(exampleRecipe);
```

## Development

For development purposes a debug service is available, start the service by running `yarn start:service`

### Installation

```
$ yarn install
$ yarn build
```
