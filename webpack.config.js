require('dotenv').config();

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: [
    'whatwg-fetch',
    './.build/renderer/index.js'
  ],
  output: {
    path: path.join(__dirname, './dist'),
    filename: 'app.js',
    library: ['Renderer'],
    libraryTarget: 'umd'
  },
  resolve: {
    extensions: ['.jsx', '.js']
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: ExtractTextPlugin.extract({
          use: [{
            loader: "css-loader"
          },{
            loader: "sass-loader"
          }],
        })
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'app.css'
    }),
    new webpack.EnvironmentPlugin([]),
  ]
};
