import * as PDF from '../lib';

require('dotenv').config();

import * as express from 'express';
import { generatePDF } from '../';

const app = express();

app.use(express.static('dist'));
app.set('view engine', 'ejs');

function getDebugRecipe (id) {
  return require(`../../${id}.json`);
}

app.get('/render/:id/:type', async (req, res) => {
  try {
    const mockRecipe = getDebugRecipe(req.params.id);
    if (req.params.type === 'toc') {
      const pdfPagesBuffer = await PDF.generatePartialDocument('pages', mockRecipe);
      const index = await PDF.generateTableOfContents(pdfPagesBuffer);
      const recipe = Object.assign({}, mockRecipe, { index });
      res.render('renderer-debug', {
        payload: recipe,
        type: req.params.type,
      });
    }
    if (req.params.type === 'pages' || req.params.type === 'frontPage') {
      const recipe = Object.assign({}, mockRecipe);
      res.render('renderer-debug', {
        payload: recipe,
        type: req.params.type,
      });
    }
  } catch (exception) {
    console.error(exception);
    res.json({
      error: true,
    });
  }
});

app.get('/render', (req, res) => {
  res.render('renderer');
});

app.get('/print/:id', async (req, res) => {
  try {
    const mockRecipe = getDebugRecipe(req.params.id);
    const recipe = Object.assign({}, mockRecipe);
    const pdf = await generatePDF(recipe);
    return res.end(pdf, 'binary');
  } catch (exception) {
    console.error(exception);
    res.json({
      error: true,
    });
  }
});

app.listen(process.env['SERVICE_PORT'], () => {
  console.log(`Debug Service listening on port ${process.env['SERVICE_PORT']}`);
});
