import * as PDF from './lib';

export async function generatePDF (recipe) {
  if (!recipe.margin) {
    recipe.margin = [2, 2, 2, 2];
  }
  if (!recipe.format) {
    recipe.format = 'A4';
  }
  const pdfFrontPageBuffer = recipe.frontPage && await PDF.generatePartialDocument('frontPage', recipe);
  const pdfPagesBuffer = await PDF.generatePartialDocument('pages', recipe);
  const index = recipe.tableOfContentsIndexes && await PDF.generateTableOfContents(recipe, pdfPagesBuffer);
  const pdfTocBuffer = recipe.tableOfContentsIndexes && await PDF.generatePartialDocument('toc', { ...recipe, index });
  return PDF.process(recipe, pdfFrontPageBuffer, pdfTocBuffer, pdfPagesBuffer);
}
