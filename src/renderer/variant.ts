import * as get from 'lodash/get';

export function createVariantClasses (classes, variants) {
  if (!variants) {
    return [];
  }
  return variants
    .map((variant) => {
      if (!classes[variant]) {
        // console.warn('No class for `variant`:', variant);
      }
      return classes[variant];
    })
    .filter(className => className);
}

export function getThemeValue (theme, component, property) {
  const customPath = propertyPath(component, property);
  const defaultPath = propertyPath('defaults', property);
  return get(theme, customPath) || get(theme, defaultPath) || null;
}

function propertyPath (...args) {
  return args.join('.');
}

export function composeComponentTheme (theme, component) {
  return {
    container: theme.components[component],
    variants: theme.variants[component],
  };
}
