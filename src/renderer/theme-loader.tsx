import * as React from 'react';
import injectSheet, { ThemeProvider, SheetsRegistry, JssProvider, jss } from 'react-jss';

const sheets = new SheetsRegistry();
const jssInsertionPointRef = React.createRef();

export class JSSInsertionPoint extends React.Component<any, any> {

  render () {
    return (
      <div ref={jssInsertionPointRef as any}>
      </div>
    );
  }

}

export class ThemeLoader extends React.Component<any, any> {

  constructor (props) {
    super(props);
    this.state = {
      insertionPoint: jssInsertionPointRef,
    };
  }

  componentDidMount () {
    jss.setup({
      insertionPoint: jssInsertionPointRef.current,
    });
    this.setState({
      insertionPoint: jssInsertionPointRef,
    });
  }

  render () {
    const { theme } = this.props;
    if (!this.state.insertionPoint || !this.state.insertionPoint.current) {
      return null;
    }
    const { children } = this.props;
    return (
      <JssProvider registry={sheets}>
        <ThemeProvider theme={theme}>
          {children}
        </ThemeProvider>
      </JssProvider>
    );
  }

}
