import * as React from 'react';
import injectSheet from 'react-jss';
import { styles } from './style';
import { ComponentFactory } from './component-factory';
import { Block, TableOfContents, Frontpage } from './components';
import * as FormatParser from '../lib/format-parser';

function ViewComponent (props) {
  const { classes, recipe, type } = props;
  const { blocks, index, frontPage } = FormatParser.parse(recipe);
  const tableOfContents = FormatParser.createTableOfContents(recipe, blocks, index);
  switch (type) {
    case 'toc':
      return (
        <div className={classes.container}>
          <TableOfContents>
            {tableOfContents}
          </TableOfContents>
        </div>
      );
    case 'pages':
      return (
        <div className={classes.container}>
          {blocks.map((block, index) => {
            return (
              <Block
                key={index}
              >
                {block.elements.map(ComponentFactory(recipe))}
              </Block>
            );
          })}
        </div>
      );
    case 'frontPage':
      return (
        <div className={classes.container}>
          <Frontpage
            key={index}
          >
            {frontPage.map(ComponentFactory(recipe))}
          </Frontpage>
        </div>
      );
  }
}

export const View = injectSheet(styles)(ViewComponent);
