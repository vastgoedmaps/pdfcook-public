const variables = {
  padding: 10,
  baseFontSize: 10,
  primaryColor: 'rgb(63,81,181)',
  leftMargin: '0cm',
  indexWidth: '2cm',
};

export function defaultTheme (theme: any = {}) {
  return {
    variables,
    defaults: {
      fontFamily: [theme.fontFamily, 'Calibri', 'Arial', 'sans-serif'],
    },
    components: {
      block: {
        marginBottom: '0.5cm',
        // paddingLeft: variables.indexWidth,
      },
      header: {
        fontSize: '1rem',
        marginBottom: '1rem',
      },
      paragraph: {
        fontSize: '1rem',
        marginBottom: '1.25rem',
        lineHeight: '1.4rem',
        whiteSpace: 'pre-wrap',
      },
      index: {
        width: variables.indexWidth,
        // marginLeft: `-${variables.indexWidth}`,
        minHeight: '1px',
        float: 'left',
        color: '#555',
      },
      html: {
        fontSize: '1rem',
        lineHeight: '1.4rem',
        marginBottom: '1.25rem',
        // whiteSpace: 'pre-wrap',
        '& ol': {
          listStyle: 'decimal',
          marginLeft: '1cm',
          marginTop: '0.25cm',
          marginBottom: '0.25cm',
          '& li': {
            fontSize: '1rem',
          },
        },
        '& ul': {
          listStyle: 'disc',
          marginLeft: '1cm',
          marginTop: '0.25cm',
          marginBottom: '0.25cm',
          '& li': {
            fontSize: '1rem',
          },
        },
        '& table': {
          pageBreakAfter: 'auto',
          '& tr': {
            pageBreakInside: 'avoid',
            pageBreakAfter: 'auto',
          },
          '& td': {
            pageBreakInside: 'avoid',
            pageBreakAfter: 'auto',
            padding: '3px',
            fontSize: '1rem',
          },
        },
        '& p': {
          fontSize: '1rem',
        },
      },
      table: {
        marginBottom: '1.25rem',
        pageBreakAfter: 'auto',
        width: '100%',
        '& tr': {
          display: 'flex',
          flexDirection: 'row',
          pageBreakInside: 'avoid',
          pageBreakAfter: 'auto',
          '&:first-child > td': {
            paddingTop: 0,
          },
        },
        '& td': {
          flex: '1',
          whiteSpace: 'pre-wrap',
          lineHeight: '1.5rem',
          padding: theme.tablePadding || '5px',
          pageBreakInside: 'avoid',
          pageBreakAfter: 'auto',
          '&:first-child': {
            paddingLeft: 0,
          },
        },
      },
    },
    variants: {
      paragraph: {
        emphasis: {
          fontWeight: 'bold',
        },
      },
      header: {
        h1: {
          fontSize: '1.8rem',
          fontWeight: 'bold',
        },
        h2: {
          fontSize: '1.4rem',
        },
        h3: {
          fontSize: '1.2rem',
        },
        hero: {
          fontSize: '2.2em',
        },
        herosub: {
          fontSize: '1.8em',
        },
      },
      tableOfContents: {

      },
      tableOfContentsRowTitle: {
        level1: {
          fontSize: '1rem',
          fontWeight: 'bold',
          marginTop: '0.25cm',
        },
        level2: {
          fontSize: '1rem',
        },
      },
      tableOfContentsRowIndexLabel: {
        level1: {
          fontSize: '1rem',
          fontWeight: 'bold',
          marginTop: '0.25cm',
          marginLeft: variables.leftMargin,
        },
        level2: {
          fontSize: '1rem',
          marginLeft: variables.leftMargin,
        },
      },
      tableOfContentsRowPageNumber: {
        level1: {
          fontSize: '1rem',
          fontWeight: 'bold',
          marginTop: '0.25cm',
        },
        level2: {
          fontSize: '1rem',
        },
      },
    },
  };
}
