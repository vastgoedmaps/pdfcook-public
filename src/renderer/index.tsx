import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Container } from './container';

export function render (props) {
  ReactDOM.render(
    <Container
      {...props}
    />,
    document.getElementById('container'),
  );
}
