import * as React from 'react';
import injectSheet from 'react-jss';
import * as classNames from 'classnames';
import { styles } from './style';
import { createVariantClasses } from '../../variant';
import * as showdown from 'showdown';

const converter = new showdown.Converter();

function ParagraphComponent (props) {
  const { key, classes, children, variants } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  const html = {
    __html: converter.makeHtml(children),
  };
  return (
    <div
      key={key}
      className={className}
      dangerouslySetInnerHTML={html}
    >
    </div>
  );
}

export const Paragraph = injectSheet(styles)(ParagraphComponent);
