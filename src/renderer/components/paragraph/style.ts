import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'paragraph');
  return {
    container: {
      fontFamily: getThemeValue(theme, 'paragraph', 'fontFamily'),
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
