import * as React from 'react';
import injectSheet from 'react-jss';
import * as classNames from 'classnames';
import { styles } from './style';
import { createVariantClasses } from '../../variant';
import { IndexLabel } from '..';

function TableComponent (props) {
  const { key, classes, children, variants } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <table
      key={key}
      className={className}
    >
      {children.map((row, index) => {
        return (
          <tr
            key={index}
          >
            {row.map((cell, index) => {
              return (
                <td
                  key={index}
                >
                  {cell}
                </td>
              );
            })}
          </tr>
        );
      })}
    </table>
  );
}

export const Table = injectSheet(styles)(TableComponent);
