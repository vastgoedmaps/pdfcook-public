import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'table');
  return {
    container: {
      fontFamily: getThemeValue(theme, 'table', 'fontFamily'),
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
