import * as React from 'react';
import injectSheet from 'react-jss';
import { styles } from './style';

function HTMLComponent (props) {
  const { key, classes, children } = props;
  const html = {
    __html: children,
  };
  return (
    <div
      key={key}
      className={classes.container}
      dangerouslySetInnerHTML={html}
    >
    </div>
  );
}

export const HTML = injectSheet(styles)(HTMLComponent);
