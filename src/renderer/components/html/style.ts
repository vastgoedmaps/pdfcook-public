import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'html');
  return {
    container: {
      fontFamily: getThemeValue(theme, 'html', 'fontFamily'),
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
