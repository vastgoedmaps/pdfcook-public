import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'index');
  return {
    container: {
      fontFamily: getThemeValue(theme, 'index', 'fontFamily'),
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
