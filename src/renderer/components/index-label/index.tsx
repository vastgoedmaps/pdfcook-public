import * as React from 'react';
import injectSheet from 'react-jss';
import * as classNames from 'classnames';
import { styles } from './style';
import { createVariantClasses } from '../../variant';

function IndexComponent (props) {
  const { key, classes, children, variants } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div
      key={key}
      className={className}
    >
      {children && children
        .filter(child => child)
        .map((child) => {
          return child
            .toString()
            .split('')
            .join('');
        })
        .join('.')
      }
    </div>
  );
}

export const IndexLabel = injectSheet(styles)(IndexComponent);
