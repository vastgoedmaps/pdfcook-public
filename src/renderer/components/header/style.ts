import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'header');
  return {
    container: {
      fontFamily: getThemeValue(theme, 'header', 'fontFamily'),
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
