import * as React from 'react';
import injectSheet from 'react-jss';
import * as classNames from 'classnames';
import { styles } from './style';
import { createVariantClasses } from '../../variant';
import { IndexLabel } from '..';

function HeaderComponent (props) {
  const { key, recipe, classes, children, variants, index } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div
      key={key}
      className={className}
    >
      <IndexLabel>{index}</IndexLabel>
      {children}
    </div>
  );
}

export const Header = injectSheet(styles)(HeaderComponent);
