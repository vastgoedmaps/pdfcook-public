import * as React from 'react';
import injectSheet from 'react-jss';
import * as classNames from 'classnames';
import { styles } from './style';
import { createVariantClasses } from '../../variant';

function FrontpageComponent (props) {
  const { key, recipe, classes, children, variants, index } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div
      key={key}
      className={className}
    >
      {children}
    </div>
  );
}

export const Frontpage = injectSheet(styles)(FrontpageComponent);
