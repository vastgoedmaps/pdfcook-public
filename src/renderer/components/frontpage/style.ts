import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'frontpage');
  return {
    container: {
      fontFamily: getThemeValue(theme, 'frontpage', 'fontFamily'),
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
