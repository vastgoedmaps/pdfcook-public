import { getThemeValue, composeComponentTheme } from '../../variant';

export const styles = {
  wrapper: (theme) => {
    const componentTheme = composeComponentTheme(theme, 'tableOfContents');
    return {
      container: {
        fontFamily: getThemeValue(theme, 'tableOfContents', 'fontFamily'),
        ...componentTheme.container,
        pageBreakAfter: 'always',
      },
      ...componentTheme.variants,
    };
  },
  row: (theme) => {
    const componentTheme = composeComponentTheme(theme, 'tableOfContentsRow');
    return {
      container: {
        fontFamily: getThemeValue(theme, 'tableOfContentsRow', 'fontFamily'),
        ...componentTheme.container,
        display: 'flex',
      },
      ...componentTheme.variants,
    };
  },
  indexLabel: (theme) => {
    const componentTheme = composeComponentTheme(theme, 'tableOfContentsRowIndexLabel');
    return {
      container: {
        fontFamily: getThemeValue(theme, 'tableOfContentsRowIndexLabel', 'fontFamily'),
        ...componentTheme.container,
      },
      ...componentTheme.variants,
    };
  },
  title: (theme) => {
    const componentTheme = composeComponentTheme(theme, 'tableOfContentsRowTitle');
    return {
      container: {
        fontFamily: getThemeValue(theme, 'tableOfContentsRowTitle', 'fontFamily'),
        ...componentTheme.container,
        flex: 1,
      },
      ...componentTheme.variants,
    };
  },
  pageNumber: (theme) => {
    const componentTheme = composeComponentTheme(theme, 'tableOfContentsRowPageNumber');
    return {
      container: {
        fontFamily: getThemeValue(theme, 'tableOfContentsRowPageNumber', 'fontFamily'),
        ...componentTheme.container,
      },
      ...componentTheme.variants,
    };
  },
};
