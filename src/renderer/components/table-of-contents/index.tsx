import * as React from 'react';
import injectSheet from 'react-jss';
import * as classNames from 'classnames';
import { styles } from './style';
import { createVariantClasses } from '../../variant';
import { IndexLabel } from '..';

function TableOfContentsRowIndexLabelComponent (props) {
  const { key, classes, variants, children } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div className={className}>
      <IndexLabel>{children}</IndexLabel>
    </div>
  );
}

function TableOfContentsRowTitleComponent (props) {
  const { key, classes, variants, children } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div className={className}>
      {children}
    </div>
  );
}

function TableOfContentsRowPageNumberComponent (props) {
  const { key, classes, variants, children } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div className={className}>
      {children}
    </div>
  );
}

function TableOfContentsRowComponent (props) {
  const { key, classes, variants, index, title, pageNumber } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div
      key={key}
      className={className}
    >
      <TableOfContentsRowIndexLabel variants={variants}>{index}</TableOfContentsRowIndexLabel>
      <TableOfContentsRowTitle variants={variants}>{title}</TableOfContentsRowTitle>
      <TableOfContentsRowPageNumber variants={variants}>{pageNumber}</TableOfContentsRowPageNumber>
    </div>
  );
}

function TableOfContentsComponent (props) {
  const { key, classes, children, variants } = props;
  const variantClasses = createVariantClasses(classes, variants);
  const className = classNames(classes.container, ...variantClasses);
  return (
    <div
      key={key}
      className={className}
    >
      {children.map((row) => {
        const { variants } = row;
        const variantClasses = createVariantClasses(classes, variants);
        const className = classNames(classes.row, ...variantClasses);
        return (
          <TableOfContentsRow
            {...row}
            variants={variants}
          />
        );
      })}
    </div>
  );
}

export const TableOfContents = injectSheet(styles.wrapper)(TableOfContentsComponent);
export const TableOfContentsRow = injectSheet(styles.row)(TableOfContentsRowComponent);
export const TableOfContentsRowIndexLabel = injectSheet(styles.indexLabel)(TableOfContentsRowIndexLabelComponent);
export const TableOfContentsRowTitle = injectSheet(styles.title)(TableOfContentsRowTitleComponent);
export const TableOfContentsRowPageNumber = injectSheet(styles.pageNumber)(TableOfContentsRowPageNumberComponent);
