import { composeComponentTheme } from '../../variant';

export const styles = (theme) => {
  const componentTheme = composeComponentTheme(theme, 'block');
  return {
    container: {
      ...componentTheme.container,
    },
    ...componentTheme.variants,
  };
};
