import * as React from 'react';
import { ThemeLoader, JSSInsertionPoint } from './theme-loader';
import { View } from './view';
import '../../css/base.scss';

import { defaultTheme } from './themes/default';

export function Container (props) {
  const { recipe } = props;
  return (
    <React.Fragment>
      <JSSInsertionPoint />
      <ThemeLoader theme={defaultTheme(recipe.theme)}>
        <View
          {...props}
        />
      </ThemeLoader>
    </React.Fragment>
  );
}
