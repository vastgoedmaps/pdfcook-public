import * as React from 'react';
import * as Components from './components';

const supportedComponents = {};

enum ComponentType {
  Header = 'header',
  Paragraph = 'paragraph',
  HTML = 'html',
  Table = 'table',
}

function registerComponent (type: ComponentType, component: React.Component) {
  supportedComponents[type] = component;
}

export function ComponentFactory (recipe) {
  return (props, index) => {
    const { type } = props;
    if (supportedComponents[type]) {
      return React.createElement(
        supportedComponents[type],
        {
          recipe,
          ...props,
          key: index,
        },
        props.value,
      );
    }
    console.error('No component found for:', props);
    return null;
  };
}

registerComponent(ComponentType.Header, Components.Header);
registerComponent(ComponentType.Paragraph, Components.Paragraph);
registerComponent(ComponentType.HTML, Components.HTML);
registerComponent(ComponentType.Table, Components.Table);
