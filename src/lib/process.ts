import * as hummus from 'hummus';
import * as stream from 'memory-streams';
import { merge } from './index';
import * as path from 'path';
import { sizes } from './render';

const fontPath = path.resolve(__dirname, '../../fonts/Open_Sans/OpenSans-Regular.ttf');

function createStyles (pdfWriter) {
  return {
    default: {
      font: pdfWriter.getFontForFile(fontPath),
      size: 8,
      colorspace: 'gray',
      color: 0x00,
    },
  };
}

function renderLayoutElement (props, recipe) {
  const { pdfWriter, context, styles, page, pageCount, tocPagesOffset, mediaBox } = props;
  
  const SPACE_UNIT = 72 / 2.54;
  const positions = {
    top: mediaBox[3] - (SPACE_UNIT * recipe.margin[0]),
    right: mediaBox[2] - (SPACE_UNIT * recipe.margin[1]),
    bottom: SPACE_UNIT * recipe.margin[2],
    left: SPACE_UNIT * recipe.margin[3],
  };
  
  function parsePosition (position) {
    const x = positions[position.split('-')[0]];
    const y = positions[position.split('-')[1]];
    return {
      x,
      y,
    };
  }
  
  return (element) => {
    console.log('\tRendering layout element:', element.type, 'on page:', page);
    const { x, y } = parsePosition(element.position);
    const baseTextHeight = styles.default.font.calculateTextDimensions('Foo Bar!', styles.default.size).height;
    switch (element.type) {
      case 'text':
        const values = Object.assign([], element.values);
        values
          .reverse()
          .map((value, index) => {
            context.writeText(value, y, x + (index * baseTextHeight) + (index * 5) + -SPACE_UNIT * 0.5, styles.default);
          });
        break;
      case 'pageNumbers':
        const currentPage = (page + 1) - tocPagesOffset;
        const totalPages = pageCount - tocPagesOffset;
        const pageNumberText = eval(element.template);
        const pageNumberTextDimensions = styles.default.font.calculateTextDimensions(pageNumberText, styles.default.size);
        if (page >= tocPagesOffset) {
          context.writeText(pageNumberText, y - pageNumberTextDimensions.width, x + -SPACE_UNIT * 0.5, styles.default);
        }
        break;
      case 'image':
        context.drawImage(y - element.width, x - 15 + -SPACE_UNIT * 0.5, element.path, {
          transformation:{
            width: element.width,
            height: element.height,
            proportional: true,
          },
        });
        break;
    }
  };
}

export function process (recipe, pdfFrontPageBuffer, pdfTocBuffer, pdfPagesBuffer) {

  console.log('Post-processing documents..');

  const mergedPdfBuffer = merge(
    pdfFrontPageBuffer,
    pdfTocBuffer,
    pdfPagesBuffer,
  );

  const pdfTocReader = pdfTocBuffer && hummus.createReader(new hummus.PDFRStreamForBuffer(pdfTocBuffer));
  const offsetFrontPage = pdfFrontPageBuffer ? 1 : 0;
  const offsetToc = pdfTocBuffer ? pdfTocReader.getPagesCount() : 0;
  const tocPagesOffset = offsetFrontPage + offsetToc;

  const writer = new stream.WritableStream();
  const pdfReader = hummus.createReader(new hummus.PDFRStreamForBuffer(mergedPdfBuffer));
  const pdfWriter = hummus.createWriterToModify(
    new hummus.PDFRStreamForBuffer(mergedPdfBuffer),
    new hummus.PDFStreamForResponse(writer),
  );

  if (recipe.layoutElements && recipe.layoutElements.length > 0) {
    const styles = createStyles(pdfWriter);
    const pageCount = pdfReader.getPagesCount();
    for (let page = 0; page < pageCount; page += 1) {

      const parsedPage = pdfReader.parsePage(page);
      const mediaBox = parsedPage.getMediaBox();

      const pageModifier = new hummus.PDFPageModifier(pdfWriter, page, true);
      const context = pageModifier.startContext().getContext();
      recipe.layoutElements.forEach(renderLayoutElement({
        pdfWriter,
        context,
        styles,
        page,
        pageCount,
        tocPagesOffset,
        mediaBox
      }, recipe));
      pageModifier.endContext().writePage();
    }
  }

  pdfWriter.end();
  console.log('Done with post-processing and ended stream');
  return writer.toBuffer();

}
