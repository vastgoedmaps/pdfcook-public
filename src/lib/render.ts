import * as puppeteer from 'puppeteer';
import * as fs from 'fs';
import * as path from 'path';

declare const Renderer;

export const sizes = {
  A4: {
    width: 210,
    height: 297
  },
  A3: {
    width: 297,
    height: 420
  }
};

export async function generatePartialDocument (type, recipe) {
  console.log('Generating partial document:', type);
  console.log('\tLaunching puppeteer', type);
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-gpu',
    ],
  });
  const page = await browser.newPage();
  const content = fs.readFileSync(path.resolve(__dirname, '../../views/render.html'), 'utf8');
  await page.setContent(content);
  await page.addScriptTag({
    path: path.resolve(__dirname, '../../dist/app.js'),
  });
  await page.addStyleTag({
    path: path.resolve(__dirname, '../../dist/app.css'),
  });
  await page.addStyleTag({
    content: `
      @page {
        size: ${sizes[recipe.format].width}mm ${sizes[recipe.format].height}mm;
        margin: ${recipe.margin[0]}cm ${recipe.margin[1]}cm ${recipe.margin[2]}cm ${recipe.margin[3]}cm;
      }
      
      @media print {
        body {
          width: ${sizes[recipe.format].width}mm;
          height: ${sizes[recipe.format].height}mm;
        }
      }
    `
  });
  if (recipe.customCss) {
    await page.addStyleTag({
      content: recipe.customCss,
    });
  }
  await page.evaluate(
    (recipe, type) => {
      Renderer.render({
        recipe,
        type,
      });
    },
    recipe,
    type,
  );
  console.log('\tCreating PDF from page', type);
  const pdfBuffer = await page.pdf({
    format: recipe.format,
    printBackground: true,
  });
  await browser.close();
  return pdfBuffer;
}
