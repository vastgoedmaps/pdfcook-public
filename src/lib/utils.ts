import * as hummus from 'hummus';
import * as stream from 'memory-streams';

export function merge (...pdfs) {
  const writer = new stream.WritableStream();
  const pdfWriter = hummus.createWriter(
    new hummus.PDFStreamForResponse(writer),
  );
  pdfs
    .filter(pdf => !!pdf)
    .forEach((pdf, index) => {
      pdfWriter.appendPDFPagesFromPDF(
        new hummus.PDFRStreamForBuffer(pdf),
      );
    });
  pdfWriter.end();
  return writer.toBuffer();
}
