export function parse (data) {
  const blocks = data.content
    .filter(block => !!block)
    .reduce(calculateIndexes, [])
    .reduce(
      (blocks, element) => {
        if (!blocks[element.block]) {
          blocks[element.block] = {
            elements: [],
          };
        }
        blocks[element.block].elements.push(element);
        return blocks;
      },
      [],
    );
  return Object.assign(
    data,
    {
      blocks,
    },
  );
}

const indexMap = [
  { variant: 'h1', level: 0 },
  { variant: 'h2', level: 1 },
  { variant: 'h3', level: 2 },
];

function calculateIndexes (elements, element, index) {
  if (elements[index - 1]) {
    element.index = [
      ...elements[index - 1].index,
    ];
  } else {
    element.index = [0, 0, 0];
  }
  indexMap.forEach((pair) => {
    if (element.variants && element.variants.includes(pair.variant)) {
      for (let i = pair.level + 1; i <= 2; i += 1) {
        element.index[i] = 0;
      }
      element.index[pair.level] += 1;
    }
  });
  elements.push({
    ...element,
  });
  return elements;
}

export function createTableOfContents (recipe, blocks, index) {
  if (!index) {
    return null;
  }
  const elements = blocks.reduce(
    (elements, block) => {
      return [
        ...elements,
        ...block.elements,
      ];
    },
    [],
  );
  return index
    .map((index) => {
      const reference = index.slice(1).map(index => index + 1);
      const element = elements.find((element) => {
        for (let i = 0; i < reference.length; i += 1) {
          if (reference[i] !== element.index[i] && reference[i] !== 0) {
            return false;
          }
        }
        return true;
      });
      const baseVariant = `level${reference.length}`;
      if (!recipe.tableOfContentsIndexes.includes(`h${reference.length}`)) {
        return null;
      }
      return {
        pageNumber: index[0] + 1,
        index: reference.map(reference => reference),
        title: element && element.value,
        variants: [baseVariant],
      };
    })
    .filter(index => !!index);
}
