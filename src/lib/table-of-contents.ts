import * as pdfReader from 'pdfreader';
import { sizes } from './render';

function cleanIndexes (indexes) {
  return indexes
    .map((index) => {
      return index
        .reduce(
          (index, current) => {
            if (index.length <= 1) {
              index.push(current);
              return index;
            }
            if (current === '.') {
              index.push('');
              return index;
            }
            index[index.length - 1] += current;
            return index;
          },
          [],
        )
        .map(character => parseInt(character, 10) - 1);
    });
}

function cleanPageIndex (page, pageIndex) {
  const indexes = [];
  for (const key in page) {
    const values = Object.values(page[key]);
    // if (!values.includes('.')) {
    //  for (let i = 0; i < values.length / 2; i += 1) {
    //    values.pop(); // deal with issue where single digit index is returned twice
    //  }
    // }
    const index = [
      pageIndex + 1,
      ...values,
    ];
    indexes.push(index);
  }
  return indexes;
}

function parseIndexes (recipe, pdfBuffer) {
  return new Promise(async (resolve, reject) => {
    let currentPage = 0;
    const pages = [];
    new pdfReader.PdfReader().parseBuffer(pdfBuffer, (err, item) => {
      if (err) {
        return reject(err);
      }
      if (!item) {
        const cleanedPages = pages
          .map(cleanPageIndex)
          .filter(indexes => indexes.length > 0)
          .reduce(
            (indexes, index) => {
              return [
                ...indexes,
                ...index,
              ];
            },
            [],
          );
        resolve(cleanedPages);
      }
      if (item && item.page) {
        currentPage = item.page - 1;
        pages[currentPage] = {};
      }
      if (item && item.text && item.oc === '#545454') {
        (pages[currentPage][item.y] = pages[currentPage][item.y] || []).push(...item.text.split(''));
      }
    });
  });
}

export async function generateTableOfContents (recipe, pdfBuffer) {
  console.log('Generating index for Table of Contents');
  const parsedIndexes = await parseIndexes(recipe, pdfBuffer);
  return cleanIndexes(parsedIndexes);
}
